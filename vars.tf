variable "aws_key_name" {
 description = "The name of YOUR instance"
 default     = "owncloud"
}  
variable "aws_region" {
 description  = "The name of region in AWS"
 default      = "eu-central-1"
}  
variable "aws_ami" {
 description  = "The IMAGE of AWS"
 default      = "ami-086a09d5b9fa35dc7"
}
variable "server_port_1" {
 description  = "The port the server will use for HTTP requests"
 default      = "8080"
}
variable "server_port_2" {
 description  = "The port the server will use for HTTP"
 default      = "80"
}
variable "server_port_3" {
 description  = "The port the server will use for HTTPS requests"
 default      = "443"
}
variable "server_port_4" {
 description  = "The port the server will use for SSH"
 default      = "22"
}
variable "aws_instance_type" {
 description  = "The type of EC2 Instances to run (e.g. t2.micro)"
 default      = "t2.micro"
}