# Terraform file for project OwnCloud on AWS, uses for Free account
# Creator Marina M.
provider "aws" {
 region          =  "${var.aws_region}"
}
resource "aws_security_group" "owncloud" {
 name = "owncloud-security-group"
 ingress {
   from_port    = "${var.server_port_1}"
   to_port      = "${var.server_port_1}"
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"]
}

 ingress {
   from_port    = "${var.server_port_2}"
   to_port      = "${var.server_port_2}"
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"]
}

 ingress {
   from_port    = "${var.server_port_3}"
   to_port      = "${var.server_port_3}"
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"]
}


 ingress {
   from_port    = "${var.server_port_4}"
   to_port      = "${var.server_port_4}"
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"]
}
 
 
 egress {
   from_port    = "${var.server_port_1}"
   to_port      = "${var.server_port_1}"
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"]
}

 egress {
   from_port    = "${var.server_port_2}"
   to_port      = "${var.server_port_2}"
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"]
}

 egress {
   from_port    = "${var.server_port_3}"
   to_port      = "${var.server_port_3}"
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"]
}

 egress {
   from_port    = "${var.server_port_4}"
   to_port      = "${var.server_port_4}"
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"]
}
resource "aws_instance" "owncloud" {
 ami              =  "${var.aws_ami}"
 instance_type    =  "${var.aws_instance_type}"
 key_name         =  "${var.aws_key_name}"
 vpc_security_group_ids = ["${aws_security_group.instance.id}"]
 
 
 user_data = <<-EOF
  #!/bin/bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install -y openssh-server
       EOF
 
 
 
 
 connection {
   type           = "ssh"
   user           = "ubuntu"
   private_key    = "${file("${var.aws_key_name}.pem")}"
   timeout        = "3m"
}


 
  provisioner "file" {
   source      = "provision.sh"
   destination = "./tmp/provision.sh"
 }
 provisioner "remote-exec" {
   inline = [
     "chmod +x /tmp/provision.sh",
     "/tmp/provision.sh ubuntu owncloud owncloud  ",
   ]
 }
 
user_data       =  "${file("provision.sh")}"
 tags {
   Name           =  "owncloud"
   
 }
}
}